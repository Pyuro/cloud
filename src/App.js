import React from "react";
import Info from "./components/info"
import Form from "./components/form"
import Weather from "./components/weather"

const API_KEY = "3e89ce6e59c6232cf92467ac46eb9707"

class App extends React.Component{

    gettingWeather = async (event) =>{
        event.preventDeafult();
        const city = event.target.elements.city.value;
        const api_url = await fetch(`api.openweathermap.org/data/2.5/weather?q=${city}&appid=${API_KEY}`)
        const data = await api_url.json();
        console.log(data);
    }

    render()
     {
        return(
            <div>
                <Info />
                <Form weatherMethod={this.gettingWeather} />
                <Weather />
            </div>
        );
    }
}

export default App;